# DataVisual

Data visualization project presenting how employment rates are related to education levels.


## Requirements

* Python3 

## Setup

1. Unzip `VisualizationProject.zip` in target directory
2. In terminal, change directory to `/VisualizationProject`, run `python3 -m http.server` command
3. Run `http://0.0.0.0:8000/index.html` in your browswer to view the project

### VisualizationProject.zip layout 

After unzip `VisualizationProject.zip`, folder should contain files as below

```
VisualizationProject/
├── ABOUT THIS TEMPLATE.txt
├── css
├── data
├── fonts
├── img
├── index.html
├── js
```

